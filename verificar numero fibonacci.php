<?php
// verificar si es cuadrado perfecto
function cuadradoPerfecto($num){
    $result = intval(sqrt($num));//calculamos la raiz cuadrada del numero y lo transformamos a Integer
    return ($result * $result == $num);//si multiplicamos el valor de la raiz por si mismo y es igual al numero ingresado, es un cuadrado perfecto.
}
// funcion para verificar si es fibonacci
function fibonacciNum($n){
    return cuadradoPerfecto(5 * $n * $n + 4) || cuadradoPerfecto(5 * $n * $n - 4);// si cumple con 5*n*n + 4 o 5*n*n - 4 ó son cuadrados perfectos
}




//vista
echo "Ingresa un numero :\n";
fscanf(STDIN, '%d\n', $numero);//ingreso del numero
//$numero=1;
if(fibonacciNum($numero)){
    echo "El numero ".$numero." es Fibonacci";
}else{
    echo "El numero ".$numero." no es Fibonacci" ;
}
?>