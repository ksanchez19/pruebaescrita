<?php
$arreglo = array (
 1,
 2,
 5,
 8,
 array(2,3,array(1,2))
 );
 
 
function arraySum($numArray){
    $suma=0;//variable que almacenara la suma
    foreach($numArray as $num){//recorremos el array
        $suma+=is_array($num) ? arraySum($num):$num;//verificamos si es un numero o un array, si es un numero se suma directamente, si es un array llamamos a nuestra funcion arraySum para que lo recorra y sume
    }
    return $suma;
}

echo arraySum($arreglo);

?>